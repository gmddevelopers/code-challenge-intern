Hello Intern Developer,

Glacier Media Digital is looking for a React Intern, maybe it's you!

Before we all get too excited, we have a brief coding challenge for you to make sure you have the skills to pay the bills.

When you have completed the code challenge, we will be in touch to review. Thanks!

## Instructions
------
_ Please read these instructions carefully!_

1. You are welcome to use any resources you like. - You can use an open book while working so why not? 
1. We would like to see how you use git. Please use features branch to work on the challenge. - Once you are working as software developer, you will be very friendly with git. So why don't you learn it while working as an intern. First, we want to make sure you have basic git skills.
1. Please work for **Two hours without any interruption and distractions** - You don't have to finish the challenge but we will track your time with timestamps on your commits to get a feel for how long things took.
1. Do the best work you can. - Show off that you are a stand out candidate 
1. **Document!** - Use as many comments as you can to describe whatever you have worked on. Tell us what your options are, why you took the path, why you didn't take another path etc! We would like to know how you think while working on the project. 

**Note:** We encourage you to use any resources you can use; However, copying and pasting large code or having somebody else do your work will result in your not being selected or being dismissed from your internship. And yes, we can tell from your code!

## The Challenges 
------
Our React app is showing a list of posts that are received from the server, however we want to add some features. The developer who is in charge of the project just left for vacation but our due date is soon. 
We would like you to integrate the following three features into the existing project. 
Our app uses React Hook for state management.

**Note:** You don't have to call API call to post data

### Challenge 1
branch: features/ch-1

We want `add` post functionality. 

We want to show the post on the screen when the user presses the add button.

### Challenge 2
branch: features/ch-2

We want `delete` post functionality. 

we want to delete the selected post from the screen when the user presses the delete button.

### Challenge 3
branch: features/ch-3

We want to show only userId 5's posts. 

When the user presses the User ID 5 button, it will only show posts from user id 5. 

------
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
