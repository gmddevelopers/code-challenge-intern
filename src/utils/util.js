import axios from 'axios';

export const initiation = (cb) => {
  axios({
    method: 'GET',
    url: 'https://jsonplaceholder.typicode.com/posts'
  }).then((res)=>{
    cb(res.data);

  }).catch((err)=>{
    cb(null, err);
  })
}
