import React, { useContext, useState, useEffect } from "react";
import {StoreContext} from '../context/StoreContext';


function Header(props){
  const { state, dispatch, actions } = useContext(StoreContext);

  return(
    <div className="header-component">
      <h3>Posts</h3>
    </div>
  );
}



export default Header;
