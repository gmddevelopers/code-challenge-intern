import React, { useContext, useState, useEffect } from "react";
import {StoreContext} from '../context/StoreContext';


function Footer(props){
  const { state, dispatch, actions } = useContext(StoreContext);

  return(
    <div className="footer-component">
      <h3>Footer</h3>
    </div>
  );
}



export default Footer;
