import React, { useContext, useState, useEffect } from "react";
import {StoreContext} from '../context/StoreContext';


function Body(props){
  const { state, dispatch, actions } = useContext(StoreContext);

  const renderPosts = () => {
    let {data} = state;
    let results = [];
    if(!data || !Array.isArray(data)) return;
    data.forEach((post,i) => {
      results.push(
        <div className="post" key={i}>
          <div className="header">
            <h3>{post.title}</h3>
          </div>
          <div className="body">
            <p>{post.body}</p>
          </div>
          <div className="footer">
            <p>Posted by userID: {post.userId}</p>
          </div>
        </div>
      )
    });
    return results;
  }


const add = () => {
  let post = {
    "userId": 11,
    "title": "New Post",
    "body": "Yay! New Post is here :)"
  }
  actions.addPost(post);
}

const del = () => {
  let id;
  actions.deletePost(id);
}

const user5 = () => {
  actions.getPostsById(5);
}


  const renderSortbtn = () => {
    return (
      <div className="btns">
        <div className="item">
          <button onClick={()=>add()}>ADD</button>
        </div>
        <div className="item">
          <button onClick={()=>del()}>DELETE</button>
        </div>
        <div className="item">
          <button onClick={()=>user5()}>USER ID 5</button>
        </div>
      </div>
    )
  }

  return(
    <div className="body-component">
      {renderSortbtn()}
      {renderPosts()}
    </div>
  );
}



export default Body;
