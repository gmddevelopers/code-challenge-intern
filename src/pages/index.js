import React, { useContext, useState, useEffect } from "react";
import {StoreContext} from '../context/StoreContext';
import {initiation} from '../utils/util';
import '../assets/styles/styles.css';

import Header from '../components/header';
import Footer from '../components/footer';
import Body from '../components/body';

function Index(props){
  const { state, dispatch, actions } = useContext(StoreContext);
  const [showHeader, setShowHeader] = useState(true);
  const [showFooter, setShowFooter] = useState(true);
  const [init, setInit] = useState(false);

  useEffect(() => {
    if(!init){
      setInit(true);
      initiation((data, err)=>{
        if(err) console.log('INP-001', err);
        actions.setData(data);
      })
    }
  });

  const renderHeader = () => {
    if(!showHeader) return;
    return(<Header />);
  }

  const renderBody = () => {
    return(<Body />)
  }

  const renderFooter = () => {
    if(!showFooter) return;
    return(<Footer />);
  }

  return(
    <div className="index-page">
      <div className="header">
        {renderHeader()}
      </div>
      <div className="body">
        {renderBody()}
      </div>
      <div className="footer">
        {renderFooter()}
      </div>
    </div>
  );
}



export default Index;
