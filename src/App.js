import React from 'react';
import logo from './logo.svg';
import './App.css';
import { StoreProvider } from "./context/StoreContext";
import Index from './pages/index';

function App() {
  return (
    <StoreProvider>
      <div className="App">
        <Index />
      </div>
    </StoreProvider>
  );
}

export default App;
