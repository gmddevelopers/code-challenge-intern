const initialState = {
  ipConfig: {token: null},
  stores: null,
  deviceLocation: null
};

const types = {
  SET_DATA:          "SET_DATA",
  GET_POSTS_BY_ID:   "GET_POSTS_BY_ID",
  ADD_POST:          "ADD_POST",
  DELETE_POST:       "DELETE_POST"
};

const reducer = (state = initialState, action) => {
  let id;
  switch (action.type) {
    case types.SET_DATA:
      return {
        ...state, data: action.payload
      }

    case types.GET_POSTS_BY_ID:
      id = action.payload;
      return state;

    case types.ADD_POST:
      return state;

    case types.DELETE_POST:
      id = action.payload;
      return state;

    default:
      throw new Error("Unexpected action");
  }
};

export { initialState, types, reducer };
