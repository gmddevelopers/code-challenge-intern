import { types } from "./reducers";

export const useActions = (state, dispatch) => {

  const setData = (item) => {
    dispatch({type: types.SET_DATA, payload: item});
  }

  const getPostsById = (id) => {
    dispatch({type: types.GET_POSTS_BY_ID, payload: id});
  }

  const addPost = (item) => {
    dispatch({type: types.ADD_POST, payload: item});
  }

  const deletePost = (id) => {
    dispatch({type: types.DELETE_POST, payload: id});
  }

  return {
    setData,
    getPostsById,
    addPost,
    deletePost
  };
};
